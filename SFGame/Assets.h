#pragma once
#include "..\SFEngine\DefaultBehaviour.h"

class SFGAME_API Assets : public sfe::DefaultBehaviourAssets
{
public:
	Assets();
	~Assets();
	void init();
	void begin();
	void update(sf::Time delta);
	void render();
};

