#pragma once
#include "..\SFEngine\DefaultBehaviour.h"

class SFGAME_API Logic : public sfe::DefaultBehaviourLogic
{
public:
	Logic();
	~Logic();
	void init();
	void begin();
	void preUpdate(sf::Time delta);
	void update(sf::Time delta);
	void render();
};

