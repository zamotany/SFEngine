/************************************************************************/
/* Author: Pawe� Trys�a aka zamotany.                                   */
/* Don not modify this file!                                            */
/************************************************************************/
int main()
{
	/************************************************************************/
	/* Entry point must be defined for compilator to pack solution into     */
	/* static lib file (SFGame.lib).                                        */
	/* Output folder is CookedLib.                                          */
	/* Do not modify solution name, output folder and output file name      */
	/* unless you know what you are doing.                                  */
	/************************************************************************/
	return 0;
}