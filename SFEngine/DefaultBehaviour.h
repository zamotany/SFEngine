/************************************************************************/
/* Author: Pawe� Trys�a aka zamotany.                                   */
/************************************************************************/
#pragma once

#ifdef SFGAME_EXROPTS 
#define SFGAME_API __declspec( dllexport ) 
#else 
#define SFGAME_API __declspec( dllimport ) 
#endif 

#include <SFML/Graphics.hpp>
#include "Sfe.h"

namespace sfe
{
	class DefaultBehaviourAssets;
	class DefaultBehaviourLogic;
}

/************************************************************************/
/* Defines how every user-controlled classes should behave.             */
/************************************************************************/
class SFGAME_API sfe::DefaultBehaviourAssets
{
public:
	DefaultBehaviourAssets(){}
	virtual ~DefaultBehaviourAssets(){};

	//Called after constructor both of engine's internal member and inheriting classes.
	virtual void init() = 0;

	//Called after init() method in body of engine's run() method.
	virtual void begin() = 0;

	//Called every time when update occurs.
	virtual void update(sf::Time delta) = 0;

	//Called every time when rendering occurs. Might be called more times than update() method.
	//Usualy ration between render() and update() is grater than 1.
	virtual void render() = 0;
};

/************************************************************************/
/* Defines how every user-controlled classes should behave.             */
/************************************************************************/
class SFGAME_API sfe::DefaultBehaviourLogic
{
public:
	DefaultBehaviourLogic(){}
	virtual ~DefaultBehaviourLogic(){};

	//Called after constructor both of engine's internal member and inheriting classes.
	virtual void init() = 0;

	//Called after init() method in body of engine's run() method.
	virtual void begin() = 0;
	
	//Called every time when update occurs before ASSET class updates.
	virtual void preUpdate(sf::Time delta) = 0;

	//Called every time when update occurs.
	virtual void update(sf::Time delta) = 0;

	//Called every time when rendering occurs. Might be called more times than update() method.
	//Usualy ration between render() and update() is grater than 1.
	virtual void render() = 0;
};
