#include "Sfe.h"
#include "SfEngine.h"

sf::RenderWindow* Window()
{
	return sfe::SfEngine::GetEngineWindow();
}

unsigned int GetWidth()
{
	return Window()->getSize().x;
}

unsigned int GetHeight()
{
	return Window()->getSize().y;
}

sfe::ResourceManager* Resources()
{
	return sfe::SfEngine::GetEnigneResourceManager();
}