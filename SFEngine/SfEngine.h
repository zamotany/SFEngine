/************************************************************************/
/* Author: Pawe� Trys�a aka zamotany.                                   */
/************************************************************************/
#pragma once

#include "VARS.h"
#include "core/INIParser.h"
#include "core/InputController.h"
#include "resources/ResourceManager.h"

namespace sfe
{
	class SfEngine;
}

class sfe::SfEngine
{
private:
	int StatusCode_;
	INIParser EngineSettings_;
	Logger Log_;

	ASSETS Assets;
	LOGIC Logic;

	sf::RenderWindow Window_;
	sf::ContextSettings Context_;
	sf::Time FPS_;

	ResourceManager Resources_;
	InputController Input_;

	void render_();
	void update_(sf::Time deltaTime);
	void processEvents_();
public:
	SfEngine();
	~SfEngine();
	int run();

	static sf::RenderWindow* GetEngineWindow();
	static sfe::ResourceManager* GetEnigneResourceManager();
	static sfe::InputController* GetEngineInputController();
};