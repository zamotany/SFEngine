#pragma once

#include <string>
#include <fstream>
#include <queue>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <filesystem>
#include <thread>
#include <mutex>
#include <condition_variable>

namespace sfe
{
	class Logger;
}

// \brief Asynchronous debug printing and loggig tool.
// \author Pawe� Trys�a aka zamotany.
class sfe::Logger
{
public:
	// \brief Default constructor.
	// \param path Path to output folder.
	// \note Name of file containging debug output will be set to default: DebugOut.%t.txt.
	Logger(const std::string& path);

	// \brief Default constructor.
	// \param path Path to output folder.
	// \param filename Name of file to which output will be written.
	Logger(const std::string& path, const std::string& filename);

	// \breif Default destructor.
	~Logger();

	// \breif Operator =.
	Logger& operator=(const Logger&) = delete;

	// \breif Copy constructor.
	Logger(const Logger&) = delete;

	// \brief Write message into debug file.
	// \param message Message to be written.
	// \note Availe macros:
	//       %y - year,
	//       %m - month,
	//       %d - day,
	//       %h - hour,
	//       %i - minutes,
	//       %s - seconds,
	//       %a - abnormal termination (ABORT),
	//       %t - shortcut for %y-%m-%d %h-%i-%s.
	// \return Itself.
	Logger& out(const std::string& message);

	// \brief Abort program and write message to file.
	// \param message Message to write.
	// \note Availe macros:
	//       %y - year,
	//       %m - month,
	//       %d - day,
	//       %h - hour,
	//       %i - minutes,
	//       %s - seconds,
	//       %a - abnormal termination (ABORT),
	//       %t - shortcut for %y-%m-%d %h-%i-%s.
	void abort(const std::string& message);
private:
	std::string Path_;
	std::string FilenamePattern_;
	std::queue<std::string> Logs_;
	std::thread Worker_;
	std::mutex Mutex_;
	std::condition_variable BreakCond_;
	bool Exit_;

	// \brief Resolve macro.
	// \param macro Macro to be resolved.
	// \return Resolved string.
	std::string resolveMacro(std::string macro);

	// \brief Resolve message.
	// \param Message to be resolved.
	// \return Resolved message.
	std::string resolveMessage(std::string message);

	// \breif Make directory if needed.
	// \param path Path to directory.
	void makeDir(std::string path);

	// \brief Resolve path.
	// \param path Path to be resolved.
	// \return Resolved path.
	std::string resolvePath(std::string path);

	// \breif Worker entry function.
	void backgroundLogging();
};

