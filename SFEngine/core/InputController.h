#pragma once

#include <functional>
#include <string>
#include <map>
#include <SFML\Graphics.hpp>
#include "INIParser.h"

namespace sfe
{
	class InputController;
}

// \brief Handler for all input.
// \author Pawe� Trys�a aka zamotany.
// \details Mapping is relation between input event and action.
//          When input event occurs, action conneted to that event will
//          be called.
//          Binding is relation between action and function
//          (aka callback). When action is called, function conneted
//          to that action is run.
//          Typical pipe line looks like this:
//          input event -> action -> function (callback).
// \note Scroll event is an exception, since it does not need any
//       mapping, so pipe line for MOUSE_SCROLLED look like this:
//       scroll (input) event -> function (callback).
class sfe::InputController
{
public:
	// \breif Enum with types of input event.
	enum class InputEventType
	{
		KEY_PRESSED = 0,
		KEY_RELEASED,
		KEY_REALTIME,
		MOUSE_BUTTON_PRESSED,
		MOUSE_BUTTON_RELEASED,
		MOUSE_SCROLLED
	};

	// \breif Struct with input event data.
	struct InputEventData
	{
		InputEventType Type;
		sf::Event::KeyEvent Key;
		sf::Event::MouseButtonEvent MouseButton;
		sf::Event::MouseWheelScrollEvent MouseScroll;
	};

	// \breif Default constructor.
	// \param mappingFile INI file with input to action mappings.
	// \note Constructor actually uses reset() method for reading mapping file.
	InputController(const std::string& mappingFile);

	// \brief Default construcor.
	~InputController();

	// \breif Copy constructor.
	// \attention This constructor is deleted.
	InputController(const InputController&) = delete;

	// \breif Operator =.
	InputController& operator=(const InputController&) = delete;

	// \breif Reset mappings.
	// \param mappingFile INI file with input to action mappings.
	// \note Only single key code (without shift, ctrl, alt, etc.) are supported.
	// \return True if reset was successful, false otherwise.
	bool reset(const std::string& mappingFile);

	// \breif Map inout to action.
	// \param action Action to which map input.
	// \param input Input which must occurs to call action.
	void map(const std::string& action, sf::Event::KeyEvent input);
	void map(const std::string& action, sf::Event::MouseButtonEvent input);

	// \breif Remove mapping from action.
	// \param action Action to which mapping will be removed.
	void removeMapping(const std::string& action);

	// \brief Bind function to action.
	// \param action Action to which function will be binded.
	// \param eventType Type of input event.
	// \param fn Function that will be called when action occurs.
	// \note Function will be given InputEventData with event's details.
	// \note If eventType is MOUSE_SCROLLED, action does not need to have mapping set,
	//       since if scroll event occurs, function (callback) is immediately run.
	void bind(const std::string& action, InputEventType eventType, std::function<void(InputEventData)> fn);

	// \breif Remove binding from given action.
	// \param action Action of which binded function will be removed.
	// \return False if action was not found, true otherwise.
	bool unbind(const std::string& action);

	// \breif Update with event.
	// \param event SFML event.
	void update(const sf::Event& event);

	// \breif Update real-time mappings/bindings.
	void update();
private:
	struct Binding
	{
		InputEventType Type;
		std::function<void(InputEventData)> Fn;
	};
	std::map<std::string, Binding> Bindings_;
	std::map<std::string, InputEventData> Mappings_;
};