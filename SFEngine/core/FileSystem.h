// \brief Functions for file manipulations.
// \author Pawe� Trys�a aka zamotany.

#pragma once

#include <string>
#include <future>
#include <filesystem>

namespace sfe
{
	// \brief Synchronously reads file in binary mode.
	// \param filename Full filename with path.
	// \param buffer Buffer array into witch data will be put.
	// \param bufferSize Size of buffer array.
	// \param bytesRead Variable holding number of bytes read.
	// \attention If bufferSize is grater than number of bytes that actually could be read then last element is -1
	//            which is an overhead, therefor should be removed.
	// \return False if filename is empty or could not open file, otherwise true.
	bool readFile(const std::string& filename, char* buffer, size_t bufferSize, size_t& bytesRead);

	// \brief Synchronously reads file in binary mode.
	// \param filename Full filename with path.
	// \param buffer String buffer.
	// \param bufferSize Size of buffer.
	// \attention If bufferSize is grater than number of bytes that actually could be read then last element is -1
	//            which is an overhead, therefor should be removed.
	// \return False if filename is empty or could not open file, otherwise true.
	bool readFile(const std::string& filename, std::string& buffer, size_t bufferSize);

	// \brief Asynchronously reads file in binary mode.
	// \param filename Full filename with path.
	// \param bufferSize Size of buffer array.
	// \return Future with read data in string class.
	// \attention Data retrival cannot be done twice, otherwise future_error will be thrown.
	// \attention If bufferSize is grater than number of bytes that actually could be read then last element is -1
	//            which is an overhead, therefor should be removed.
	// \note Before apptempting to retrive data from future object, make sure future is valid.
	// \note If attemption to read file in new thread fails, file will be read either asynchronously or synchronously
	//       depending on avaible system resource.
	std::future<std::string> readFileAsync(const std::string& filename, size_t bufferSize);

	// \brief Alias for TR2's filesystem library.
	namespace fs = std::tr2::sys;
}