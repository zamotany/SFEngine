#include "InputController.h"

sfe::InputController::InputController(const std::string& mappingFile)
{
	reset(mappingFile);
}

sfe::InputController::~InputController()
{
}

bool sfe::InputController::reset(const std::string& mappingFile)
{
	if (mappingFile.empty())
		return false;

	sfe::INIParser file(mappingFile);
	if (file.sectionExist("Mouse"))
	{
		auto keys = file.getKeysVector("Mouse");
		for (auto& key : keys)
		{
			sf::Event::MouseButtonEvent input;
			input.x = input.y = 0;
			input.button = (sf::Mouse::Button)file.getInt("Mouse", key);
			map(key, input);
		}
	}
	if (file.sectionExist("Keyboard"))
	{
		auto keys = file.getKeysVector("Keyboard");
		for (auto& key : keys)
		{
			sf::Event::KeyEvent input = { (sf::Keyboard::Key)file.getInt("Keyboard", key), false, false, false, false };
			map(key, input);
		}
	}

	return true;
}


void sfe::InputController::map(const std::string& action, sf::Event::KeyEvent input)
{
	Mappings_[action].Type = InputEventType::KEY_PRESSED;
	Mappings_[action].Key = input;
}

void sfe::InputController::map(const std::string& action, sf::Event::MouseButtonEvent input)
{
	Mappings_[action].Type = InputEventType::MOUSE_BUTTON_PRESSED;
	Mappings_[action].MouseButton = input;
}

void sfe::InputController::removeMapping(const std::string& action)
{
	auto it = Mappings_.find(action);
	if (it != Mappings_.end())
		Mappings_.erase(it);
}

void sfe::InputController::bind(const std::string& action, InputEventType eventType, std::function<void(InputEventData)> fn)
{
	Bindings_[action].Type = eventType;
	Bindings_[action].Fn = fn;
}

bool sfe::InputController::unbind(const std::string& action)
{
	auto it = Bindings_.find(action);
	if (it == Bindings_.end())
		return false;
	Bindings_.erase(it);
	return true;
}

void sfe::InputController::update(const sf::Event& event)
{
	if (event.type != sf::Event::EventType::KeyPressed &&
		event.type != sf::Event::EventType::KeyReleased &&
		event.type != sf::Event::EventType::MouseWheelScrolled &&
		event.type != sf::Event::EventType::MouseButtonPressed &&
		event.type != sf::Event::EventType::MouseButtonReleased)
		return;

	if (event.type == sf::Event::EventType::MouseWheelScrolled)
	{
		for (auto it = Bindings_.begin(); it != Bindings_.end(); it++)
		{
			if (it->second.Type == InputEventType::MOUSE_SCROLLED)
			{
				InputEventData data;
				data.Type = it->second.Type;
				data.MouseScroll = event.mouseWheelScroll;
				it->second.Fn(data);
			}
		}
		return;
	}


	for (auto it = Mappings_.begin(); it != Mappings_.end(); it++)
	{
		
		if ((event.type == sf::Event::EventType::KeyPressed || event.type == sf::Event::EventType::KeyReleased)
			&& (it->second.Type == InputEventType::KEY_PRESSED || it->second.Type == InputEventType::KEY_RELEASED))
		{
			if (it->second.Key.code == event.key.code && it->second.Key.alt == event.key.alt
				&& it->second.Key.control == event.key.control && it->second.Key.shift == event.key.shift
				&& it->second.Key.system == event.key.system)
			{
				auto binding = Bindings_.find(it->first);
				if (binding != Bindings_.end())
				{
					if ((binding->second.Type == InputEventType::KEY_PRESSED && event.type == sf::Event::EventType::KeyPressed)
						|| (binding->second.Type == InputEventType::KEY_RELEASED && event.type == sf::Event::EventType::KeyReleased))
					{
						InputEventData data;
						data.Type = it->second.Type;
						data.Key = event.key;
						binding->second.Fn(data);
						break;
					}
				}
			}
		}
		else if ((event.type == sf::Event::EventType::MouseButtonPressed || event.type == sf::Event::EventType::MouseButtonReleased)
			&& (it->second.Type == InputEventType::MOUSE_BUTTON_PRESSED || it->second.Type == InputEventType::MOUSE_BUTTON_RELEASED))
		{
			if (it->second.MouseButton.button == event.mouseButton.button)
			{
				auto binding = Bindings_.find(it->first);
				if (binding != Bindings_.end())
				{
					if ((binding->second.Type == InputEventType::MOUSE_BUTTON_PRESSED && event.type == sf::Event::EventType::MouseButtonPressed)
						|| (binding->second.Type == InputEventType::MOUSE_BUTTON_RELEASED && event.type == sf::Event::EventType::MouseButtonReleased))
					{
						InputEventData data;
						data.Type = it->second.Type;
						data.MouseButton = event.mouseButton;
						binding->second.Fn(data);
						break;
					}
				}
			}
		}
	}
}

void sfe::InputController::update()
{
	for (auto it = Bindings_.begin(); it != Bindings_.end(); it++)
	{
		if (it->second.Type == InputEventType::KEY_REALTIME)
		{
			auto map = Mappings_.find(it->first);
			if (map != Mappings_.end())
			{
				if (sf::Keyboard::isKeyPressed(map->second.Key.code))
				{
					InputEventData data;
					data.Type = it->second.Type;
					data.Key = { map->second.Key.code, false, false, false, false };
					it->second.Fn(data);
				}
			}
		}
	}
}