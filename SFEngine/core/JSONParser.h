#pragma once

#include "rapidjson/rapidjson.h"
#include "rapidjson/document.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/writer.h"
#include <string>
#include <fstream>

namespace sfe
{
	class JSONParser;
}

// \brief Wrapper for RapidJSON library.
// \author Pawe� Trys�a aka zamotany.
// \note RapidJSON library's documentation avaible here: http://rapidjson.org/
class sfe::JSONParser
{
public:
	// \brieg Default constructor.
	JSONParser();

	// \brieg Default destructor.
	~JSONParser();

	// \brief Operator =.
	JSONParser& operator=(const JSONParser&) = delete;

	// \brief Copy constructor.
	JSONParser(const JSONParser&) = delete;

	// \brief Parse already existing JSON file.
	// \param filename Full file name with path.
	// \return False if filename is empty or could not open file, true otherwise.
	bool loadAndParse(const std::string& filename);

	// \brief Parse JSON string
	// \param json String containing JSON.
	// \param filename Full file name with path of file to which write JSON document.
	void parse(const std::string& json, const std::string& filename = "");

	// \brief Create new (empty) JSON file.
	// \param filename Full file name with path.
	void set(const std::string& filename);

	// \brief Check if root document has given member.
	// \return True if member exists, false otherwise.
	bool hasMember(const std::string& member);

	// \brief Get RapidJSON's value mapped to given member of root document.
	// \note Refer to RapidJSON's documentation for further information.
	// \return Pointer to RapidJSON's value class.
	rapidjson::Value* get(const std::string& member);
	rapidjson::Value* operator[](const std::string& member);

	// \brief Save modified JSON data back to file.
	// \note Can be saved multiple times.
	// \return False if document was newly created or there were an error while opening file, true otherwise.
	bool save();
private:
	std::string Filename_;
	std::fstream File_;
	rapidjson::Document Document_;
};

