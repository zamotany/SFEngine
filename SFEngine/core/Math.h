// \breif Common mathematic functions.
// \author Konstanty Misiak.
#pragma once
#include <SFML\Graphics.hpp>

namespace sfe
{
	// \brief Dot product (scalar multiplication) of two vectors.
	// \param v1 Vector 1.
	// \param v2 Vector 2.
	// \return Dot product.
	float dot(const sf::Vector2f& v1, const sf::Vector2f& v2);

	// \brief Get magnitude of vector.
	// \param vector Vector used to compute megnitude.
	// \return Magnitude.
	float magnitude(const sf::Vector2f& vector);

	// \brief Create unit vector (parallel vector of magnitude 1).
	// \param vector Original vector.
	// \return Unit vector.
	sf::Vector2f unitVector(const sf::Vector2f& vector);

	// \brief Scale (multiply) vector by constant.
	// \param vector Original vector.
	// \param multiplier Multiplier constant.
	// \return Scaled vector.
	sf::Vector2f scale(const sf::Vector2f& vector, float multiplier);

	// \brief Add two vectors.
	// \param v1 Vector 1.
	// \param v2 Vector 2.
	// \return New vector.
	sf::Vector2f add(const sf::Vector2f& v1, const sf::Vector2f& v2);

	// \brief Get angle between two vectors.
	// \param v1 Vector 1.
	// \param v2 Vector 2.
	// \return Angle in degrees.
	float angle(const sf::Vector2f& v1, const sf::Vector2f& v2);

	// \brief Create vector tilted by given angle to original one.
	// \param vector Original vector.
	// \param angle Angle in degrees by which new vector will be tilted.
	// \return Tilted vector.
	sf::Vector2f tilt(const sf::Vector2f& vector, float angle);

	// \brief Convert angle in radians to angle in degrees.
	// \param rad Angle in radian.
	// \return Angle in degrees.
	float toDegree(float rad);

	// \brief Convert angle in degrees to angle in radians.
	// \param degree Angle in degrees.
	// \return Angle in radians.
	float toRad(float degree);

	// \brief Get sinus value of angle.
	// \param angle Angle in degrees.
	// \return Sinus value.
	float sin(float angle);

	// \brief Get cosinus value of angle.
	// \param angle Angle in degrees.
	// \return cosinus value.
	float cos(float angle);

	// \brief Get tanges value of angle.
	// \param angle Angle in degrees.
	// \return Tanges value.
	float tg(float angle);

	// \brief Get cotanges value of angle.
	// \param angle Angle in degrees.
	// \return Cotanges value.
	float ctg(float angle);

	// \brief Get arcus sinus.
	// \param value Value of sinus.
	// \return Angle in degrees.
	float arcSin(float value);

	// \brief Get arcus cosinuss.
	// \param value Value of cosinus.
	// \return Angle in degrees.
	float arcCos(float value);

	// \brief Get arcus tanges.
	// \param value Value of tanges.
	// \return Angle in degrees.
	float arcTg(float value);

	// \brief Get arcus cosinus.
	// \param value Value of cosinus.
	// \return Angle in degrees.
	float arcCtg(float value);
}