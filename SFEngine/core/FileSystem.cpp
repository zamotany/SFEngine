#include "FileSystem.h"

bool sfe::readFile(const std::string& filename, char* buffer, size_t bufferSize, size_t& bytesRead)
{
	bytesRead = 0;

	if (filename.empty())
		return false;

	std::ifstream file(filename, std::ios_base::binary);
	if (!file.is_open())
		return false;

	size_t index = 0;
	while (file.good() && index < bufferSize)
	{
		buffer[index] = file.get();
		index++;
	}

	bytesRead = index;
	return true;
}

bool sfe::readFile(const std::string& filename, std::string& buffer, size_t bufferSize)
{
	if (filename.empty())
		return false;

	std::ifstream file(filename, std::ios_base::binary);
	if (!file.is_open())
		return false;

	size_t index = 0;
	while (file.good() && index < bufferSize)
	{
		buffer.push_back(file.get());
		index++;
	}

	return true;
}

std::future<std::string> sfe::readFileAsync(const std::string& filename, size_t bufferSize)
{
	std::future<std::string> results;
	try
	{
		results = std::async(std::launch::async, [filename, bufferSize]() -> std::string
		{
			std::string buffer;
			std::ifstream file(filename, std::ios_base::binary);

			if (file.is_open())
			{
				size_t index = 0;
				while (file.good() && index < bufferSize)
				{
					buffer.push_back(file.get());
					index++;
				}
			}
			return buffer;
		});
	}
	catch (const std::system_error&)
	{
		results = std::async(std::launch::any, [filename, bufferSize]() -> std::string
		{
			std::string buffer;
			std::ifstream file(filename, std::ios_base::binary);

			if (file.is_open())
			{
				size_t index = 0;
				while (file.good() && index < bufferSize)
				{
					buffer.push_back(file.get());
					index++;
				}
			}
			return buffer;
		});
	}
	return results;
}