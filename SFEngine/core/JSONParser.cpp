#include "JSONParser.h"

sfe::JSONParser::JSONParser()
{
}

sfe::JSONParser::~JSONParser()
{
}

bool sfe::JSONParser::loadAndParse(const std::string& filename)
{
	if (filename.empty())
		return false;

	Filename_ = filename;
	File_.open(filename, std::ios_base::in);
	if (!File_.is_open())
		return false;

	std::string json;
	std::string temp;
	while (getline(File_, temp))
	{
		json += temp;
	}
	File_.close();

	Document_.Parse(json.c_str());
	return true;
}

void sfe::JSONParser::parse(const std::string& json, const std::string& filename)
{
	Filename_ = filename;
	Document_.Parse(json.c_str());
}

void sfe::JSONParser::set(const std::string& filename)
{
	Filename_ = filename;
	Document_.SetObject();
}

bool sfe::JSONParser::hasMember(const std::string& member)
{
	return Document_.HasMember(member.c_str());
}

bool sfe::JSONParser::save()
{
	if (Filename_.empty() || !Document_.IsObject())
		return false;

	File_.open(Filename_, std::ios_base::out);
	if (!File_.is_open())
		return false;

	rapidjson::StringBuffer buffer;
	rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
	if (!Document_.Accept(writer))
		return false;

	File_ << buffer.GetString();
	File_.close();
	return true;
}

rapidjson::Value* sfe::JSONParser::get(const std::string& member)
{
	if (!Document_.IsObject() || !Document_.HasMember(member.c_str()))
		return nullptr;

	return &Document_[member.c_str()];
}

rapidjson::Value* sfe::JSONParser::operator[](const std::string& member)
{
	return get(member);
}