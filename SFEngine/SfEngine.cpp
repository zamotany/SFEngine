#include "SfEngine.h"
#include <iostream>

sfe::SfEngine game;

int main()
{
	return game.run();
}


sf::RenderWindow* sfe::SfEngine::GetEngineWindow()
{
	return &game.Window_;
}

sfe::ResourceManager* sfe::SfEngine::GetEnigneResourceManager()
{
	return &game.Resources_;
}

sfe::InputController* sfe::SfEngine::GetEngineInputController()
{
	return &game.Input_;
}

sfe::SfEngine::SfEngine() :
	EngineSettings_(ENGINE_SETTINGS),
	Resources_(EngineSettings_.get("Resources", "Config", "CookedPC/ResourceManager.ini")),
	Input_(EngineSettings_.get("Input", "Config", "CookedPC/InputMappings.ini")),
	Log_(EngineSettings_.get("Debug", "LogOutput", "Logs"), "EngineLog.%t.txt")
{
	Log_.out("[%t] Initialising enigne's internal systems.");

	StatusCode_ = 0;
	Context_.antialiasingLevel = EngineSettings_.getInt("Window", "AALevel");
	Window_.create(
		sf::VideoMode(EngineSettings_.getInt("Window", "Width", 1280), EngineSettings_.getInt("Window", "Height", 720)),
		EngineSettings_.get("Window", "Title", "NoTitle"),
		(EngineSettings_.getBool("Window", "Borderless") ? sf::Style::None : sf::Style::Titlebar | sf::Style::Close),
		Context_);
	float fps = EngineSettings_.getFloat("Window", "FramerateLimit", 0.f);
	FPS_ = fps == 0.f ? sf::Time::Zero : sf::seconds(1.f / fps);
	Window_.requestFocus();
	Window_.setActive();

	Log_.out("[%t] Initialising Game Objects, components and scripts.");

	Assets.init();
	Logic.init();
}

sfe::SfEngine::~SfEngine()
{
}

int sfe::SfEngine::run()
{
	Log_.out("[%t] Calling begin() method.");

	Assets.begin();
	Logic.begin();

	sf::Time elapsed = sf::Time::Zero;
	sf::Time delta = sf::Time::Zero;
	sf::Clock clock;
	clock.restart();

	while (Window_.isOpen())
	{
		processEvents_();

		update_(delta);
		render_();

		elapsed = clock.restart();
		if (FPS_ != sf::Time::Zero && FPS_.asSeconds() - elapsed.asSeconds() > 0.f)
			sf::sleep(FPS_ - elapsed);
		clock.restart();
		if (delta == sf::Time::Zero)
			delta = elapsed;
		else delta += sf::seconds((elapsed.asSeconds() - delta.asSeconds()) / 2);
	}

	Log_.out("[%t] Exiting with status code: " + std::to_string(StatusCode_) + '.');
	return StatusCode_;
}

void sfe::SfEngine::render_()
{
	Assets.render();
	Logic.render();
}

void sfe::SfEngine::update_(sf::Time deltaTime)
{
	Logic.preUpdate(deltaTime);
	Assets.update(deltaTime);
	Logic.update(deltaTime);
}

void sfe::SfEngine::processEvents_()
{
}