/************************************************************************/
/* Author: Pawe� Trys�a aka zamotany.                                   */
/* This file provides declaration of every function users have access   */
/* to.                                                                  */
/************************************************************************/
#pragma once

#include "core/JSONParser.h"
#include "core/INIParser.h"
#include "resources/ResourceManager.h"
#include "gpf/tmx-loader/MapLoader.h"

namespace sfe
{
	namespace tmx = tmx;
}

//Gets window handle.
sf::RenderWindow* Window();

//Shortcut for getting window's width.
unsigned int GetWidth();

//Shortcut for getting window's height.
unsigned int GetHeight();

//Gets textures manager.
sfe::ResourceManager* Resources();

//Input()
//Sounds()
//Music()
//State()
//Cameras()
//Physic()