#include "BitmaskMgr.h"

sfe::BitmaskMgr::BitmaskMgr()
{
}

sfe::BitmaskMgr::~BitmaskMgr()
{
	//Get iterator for last element.
	std::map<const sf::Texture*, sf::Uint8*>::const_iterator end = Bitmasks_.end();

	//Delete elements.
	for (std::map<const sf::Texture*, sf::Uint8*>::const_iterator it = Bitmasks_.begin(); it != end; it++)
		delete[] it->second;
}

sf::Uint8 sfe::BitmaskMgr::getPixel(const sf::Uint8* mask, const sf::Texture* tex, unsigned int x, unsigned int y)
{
	if (x > tex->getSize().x || y > tex->getSize().y)
		return 0;
	return mask[x + y * tex->getSize().x];
}

sf::Uint8* sfe::BitmaskMgr::createMask(const sf::Texture* tex, const sf::Image& img)
{
	sf::Uint8* mask = new sf::Uint8[tex->getSize().y * tex->getSize().x];

	for (unsigned int y = 0; y < tex->getSize().y; y++)
	{
		for (unsigned int x = 0; x < tex->getSize().x; x++)
			mask[x + y * tex->getSize().x] = img.getPixel(x, y).a;
	}

	Bitmasks_.insert(std::pair<const sf::Texture*, sf::Uint8*>(tex, mask));

	return mask;
}

sf::Uint8* sfe::BitmaskMgr::getMask(const sf::Texture* tex)
{
	sf::Uint8* mask;
	std::map<const sf::Texture*, sf::Uint8*>::iterator pair = Bitmasks_.find(tex);
	if (pair == Bitmasks_.end())
	{
		sf::Image img = tex->copyToImage();
		mask = createMask(tex, img);
	}
	else
		mask = pair->second;

	return mask;
}