#include "TextureMgr.h"
#include "BitmaskMgr.h"

sfe::TextureMgr::TextureMgr()
{
}

sfe::TextureMgr::~TextureMgr()
{
}

bool sfe::TextureMgr::load(const std::string& filename, const std::string& name, bool createMask)
{
	if (filename.empty() || name.empty() || Textures_.find(name) != Textures_.end())
		return false;

	if (createMask)
	{
		Textures_.emplace(name, sf::Texture());
		sf::Image img;
		if (!img.loadFromFile(filename))
			return false;
		if (!Textures_.at(name).loadFromImage(img))
			return false;

		Bitmasks.createMask(&Textures_.at(name), img);
	}
	else
	{
		Textures_.emplace(name, sf::Texture());
		if(!Textures_.at(name).loadFromFile(filename))
			return false;
	}
	
	return true;
}

bool sfe::TextureMgr::loadBinary(const std::string& data, const std::string& name, bool createMask)
{
	if (data.empty() || name.empty() || Textures_.find(name) != Textures_.end())
		return false;

	if (createMask)
	{
		Textures_.emplace(name, sf::Texture());
		sf::Image img;
		if (!img.loadFromMemory(data.c_str(), data.length()))
			return false;
		if (!Textures_.at(name).loadFromImage(img))
			return false;

		Bitmasks.createMask(&Textures_.at(name), img);
	}
	else
	{
		Textures_.emplace(name, sf::Texture());
		if (!Textures_.at(name).loadFromMemory(data.c_str(), data.length()))
			return false;
	}
	return true;
}

sf::Texture* sfe::TextureMgr::get(const std::string& name)
{
	if (name.empty() || Textures_.find(name) == Textures_.end())
		return nullptr;
	return &Textures_.at(name);
}

void sfe::TextureMgr::smooth(const std::string& name)
{
	if (name.empty() || Textures_.find(name) != Textures_.end())
		Textures_.at(name).setSmooth(true);
}
void sfe::TextureMgr::repeat(const std::string& name)
{
	if (name.empty() || Textures_.find(name) != Textures_.end())
		Textures_.at(name).setRepeated(true);
}