#pragma once

#include <fstream>
#include <map>
#include <string>
#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>

namespace sfe
{
	class BaseMgr;
}

// \brief Base class for unit managers.
// \author Pawe� Trys�a aka zamotany.
class sfe::BaseMgr
{
public:
	// \brief Default constructor.
	BaseMgr();

	// \breif Default destructor.
	~BaseMgr();
};

