#pragma once

#include "BitmaskMgr.h"

namespace sfe
{
	class TextureMgr;
}

// \breif Unit manager for textures.
// \author Pawe� Trys�a aka zamotany.
class sfe::TextureMgr : public BaseMgr
{
public:
	// \breif Default constructor.
	TextureMgr();

	// \brief Default destructor.
	~TextureMgr();

	// \breif Operator =.
	TextureMgr& operator=(const TextureMgr&) = delete;

	// \brief Copy constructor.
	TextureMgr(const TextureMgr&) = delete;

	// \brief Load texture from file.
	// \param filename Full file name with path.
	// \param name Name of the texture for identification.
	// \param createMask Whether to create bitmask.
	// \return False is filename is empty, texture is already loaded or could not load texture, true otherwise.
	bool load(const std::string& filename, const std::string& name, bool createMask);

	// \breif Load texture from memory.
	// \param data Binary data representing texture.
	// \param name Name of the texture for identification.
	// \param createMask Whether to create bitmask.
	// \return False is data is empty, texture is already loaded or could not load texture, true otherwise.
	bool loadBinary(const std::string& data, const std::string& name, bool createMask);

	// \brief Get texture specified by name.
	// \param name Name identificator of texture.
	// \return Pointer to texture.
	sf::Texture* get(const std::string& name);

	// \brief Smooth texture.
	// \param name Name identificator of texture.
	void smooth(const std::string& name);

	// \brief Repeat texture.
	// \param name Name identificator of texture.
	void repeat(const std::string& name);

	BitmaskMgr Bitmasks;
private:
	std::map<sf::String, sf::Texture> Textures_;
};