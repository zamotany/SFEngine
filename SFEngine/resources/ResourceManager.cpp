#include "ResourceManager.h"

sfe::ResourceManager::ResourceManager(const std::string& config) : Config_(config)
{
}

sfe::ResourceManager::~ResourceManager()
{
}

int sfe::ResourceManager::internalLoad_(const std::string& package)
{
	Logger log("logs", Config_.get("Debug", "OutputFile", "ResourceManager.%t.txt"));
	log.out("[%t] Start internal loading package: " + package + '.');

	if (package.empty())
	{
		log.out("[%t] Error: 1. Stopping load.");
		return 1;
	}

	JSONParser packageConfig;
	if (!packageConfig.loadAndParse(package))
	{
		log.out("[%t] Error: 2. Stopping load.");
		return 2;
	}

	std::string file(Config_.get("Package", "File", "file"));
	std::string size(Config_.get("Package", "Size", "size"));

	if (!packageConfig.hasMember(file.c_str()))
	{
		log.out("[%t] Error: 3. Stopping load.");
		return 3;
	}
	if (!packageConfig.hasMember(size.c_str()))
	{
		log.out("[%t] Error: 4. Stopping load.");
		return 4;
	}
	std::string packagePath(packageConfig[file]->GetString());
	log.out("[%t] Package path: " + packagePath +'.');
	size_t packageSize = packageConfig[size]->GetInt();
	log.out("[%t] Package size: " + std::to_string(packageSize) + '.');

	std::string packageBuffer;
	if (!sfe::readFile(packagePath, packageBuffer, packageSize))
	{
		log.out("[%t] Error: 5. Stopping load.");
		return 5;
	}

	if (packageBuffer.length() == 0)
	{
		log.out("[%t] Error: 6. Stopping load.");
		return 6;
	}

	std::istringstream packageBufferStream(packageBuffer);

	auto packageArchive = ZipArchive::Create(&packageBufferStream, false);
	if (!packageArchive)
	{
		log.out("[%t] Error: 7. Stopping load.");
		return 7;
	}

	int returnValue = 0;
	internalLoadTextures_(&packageConfig, packageArchive.get(), returnValue, &log);
	if (returnValue != 0)
		return returnValue;

	internalLoadFonts_(&packageConfig, packageArchive.get(), returnValue, &log);
	if (returnValue != 0)
		return returnValue;

	internalLoadAudio_(&packageConfig, packageArchive.get(), returnValue, &log);
	if (returnValue != 0)
		return returnValue;

	log.out("[%t] Finished loading.");
	return returnValue;
}

void sfe::ResourceManager::internalLoadTextures_(JSONParser* packageConfig, ZipArchive* archive, int& returnValue, Logger* log)
{
	std::string textures(Config_.get("Textures", "Name", "textures"));
	if (packageConfig->hasMember(textures))
	{
		log->out("[%t] Start internal textures load.");

		std::string path(Config_.get("Textures", "Path", "path"));
		if (!packageConfig->get(textures)->HasMember(path.c_str()))
		{
			log->out("[%t] Error: 8. Stopping load.");
			returnValue = 8;
		}
		std::string texturesPath(packageConfig->get(textures)->FindMember(path.c_str())->value.GetString());
		if (texturesPath.back() != '/')
			texturesPath += '/';

		log->out("[%t] Textures path: " + texturesPath + '.');

		std::string files(Config_.get("Textures", "Container", "files"));
		if (!packageConfig->get(textures)->HasMember(files.c_str()))
		{

			log->out("[%t] Error: 9. Stopping load.");
			returnValue = 9;
		}
		auto arr = packageConfig->get(textures)->FindMember(files.c_str());

		if (!arr->value.IsArray())
		{
			log->out("[%t] Error: 10. Stopping load.");
			returnValue = 10;
		}

		std::string name(Config_.get("Textures", "FileName", "name"));
		std::string alias(Config_.get("Textures", "FileAlias", "alias"));
		std::string createMask(Config_.get("Textures", "FileCreateMask", "createMask"));
		std::string repeat(Config_.get("Textures", "FileRepeat", "repeat"));
		std::string smooth(Config_.get("Textures", "FileSmooth", "smooth"));

		for (auto it = arr->value.Begin(); it != arr->value.End(); it++)
		{
			if (!it->HasMember(name.c_str()))
				continue;

			log->out("[%t] Preparing to load texture: " + std::string(it->FindMember(name.c_str())->value.GetString()));

			auto entry = archive->GetEntry(texturesPath + it->FindMember(name.c_str())->value.GetString());
			if (entry->CanExtract())
			{
				std::istream* rawData = entry->GetDecompressionStream();
				if (!rawData)
				{
					log->out("[%t] Decompression stream is null.");
					continue;
				}

				std::string data;
				while (rawData->good())
					data.push_back(rawData->get());
				data.erase(data.length() - 1);

				log->out("[%t] Loading texture.");

				{
					std::lock_guard<std::mutex> lg(TexturesMutex_);
					if (Textures_.loadBinary(data,
						(it->HasMember(alias.c_str()) ? it->FindMember(alias.c_str())->value.GetString() : it->FindMember(name.c_str())->value.GetString()),
						(it->HasMember(createMask.c_str()) ? it->FindMember(createMask.c_str())->value.GetBool() : true)
						))
					{
						log->out("[%t] Loading successful.");

						if (it->HasMember(repeat.c_str()))
						{
							if (it->FindMember(repeat.c_str())->value.GetBool())
								Textures_.repeat(it->HasMember(alias.c_str()) ? it->FindMember(alias.c_str())->value.GetString() : it->FindMember(name.c_str())->value.GetString());
						}

						if (it->HasMember(smooth.c_str()))
						{
							if (it->FindMember(smooth.c_str())->value.GetBool())
								Textures_.smooth(it->HasMember(alias.c_str()) ? it->FindMember(alias.c_str())->value.GetString() : it->FindMember(name.c_str())->value.GetString());
						}
					}
					else log->out("[%t] Error: Could not load texture: " + std::string(it->FindMember(name.c_str())->value.GetString()));
				}
			}
		}
	}

	log->out("[%t] Stoping internal textures load.");
	returnValue = 0;
}

void sfe::ResourceManager::internalLoadFonts_(JSONParser* packageConfig, ZipArchive* archive, int& returnValue, Logger* log)
{
	std::string fonts(Config_.get("Fonts", "Name", "fonts"));
	if (packageConfig->hasMember(fonts))
	{
		log->out("[%t] Start internal fonts load.");

		std::string path(Config_.get("Fonts", "Path", "path"));
		if (!packageConfig->get(fonts)->HasMember(path.c_str()))
		{
			log->out("[%t] Error: 11. Stopping load.");
			returnValue = 11;
		}
		std::string fontsPath(packageConfig->get(fonts)->FindMember(path.c_str())->value.GetString());
		if (fontsPath.back() != '/')
			fontsPath += '/';

		log->out("[%t] Fonts path: " + fontsPath + '.');

		std::string files(Config_.get("Fonts", "Container", "files"));
		if (!packageConfig->get(fonts)->HasMember(files.c_str()))
		{
			log->out("[%t] Error: 12. Stopping load.");
			returnValue = 12;
		}
		auto arr = packageConfig->get(fonts)->FindMember(files.c_str());

		if (!arr->value.IsArray())
		{
			log->out("[%t] Error: 13. Stopping load.");
			returnValue = 13;
		}

		std::string name(Config_.get("Fonts", "FileName", "name"));
		std::string alias(Config_.get("Fonts", "FileAlias", "alias"));

		for (auto it = arr->value.Begin(); it != arr->value.End(); it++)
		{
			if (!it->HasMember(name.c_str()))
				continue;

			log->out("[%t] Preparing to load font: " + std::string(it->FindMember(name.c_str())->value.GetString()));

			auto entry = archive->GetEntry(fontsPath + it->FindMember(name.c_str())->value.GetString());
			if (entry->CanExtract())
			{
				std::istream* rawData = entry->GetDecompressionStream();
				if (!rawData)
				{
					log->out("[%t] Decompression stream is null.");
					continue;
				}

				std::string data;
				while (rawData->good())
					data.push_back(rawData->get());
				data.erase(data.length() - 1);

				log->out("[%t] Loading font.");

				{
					std::lock_guard<std::mutex> lg(FontsMutex_);
					if (!Fonts_.loadBinary(data,
						(it->HasMember(alias.c_str()) ? it->FindMember(alias.c_str())->value.GetString() : it->FindMember(name.c_str())->value.GetString())))
						log->out("[%t] Error: Could not load font " + std::string(it->FindMember(name.c_str())->value.GetString()));
					else log->out("[%t] Loading successful.");
				}
			}
		}
	}

	log->out("[%t] Stoping internal fonts load.");
	returnValue = 0;
}

void sfe::ResourceManager::internalLoadAudio_(JSONParser* packageConfig, ZipArchive* archive, int& returnValue, Logger* log)
{
	std::string audio(Config_.get("Audio", "Name", "audio"));
	if (packageConfig->hasMember(audio))
	{
		log->out("[%t] Start internal audio load.");

		std::string soundPath(Config_.get("Audio", "SoundPath", "soundPath"));
		std::string musicPath(Config_.get("Audio", "MusicPath", "musicPath"));
		std::string actualSoundPath;
		std::string actualMusicPath;
		if (!packageConfig->get(audio)->HasMember(soundPath.c_str()) && !packageConfig->get(audio)->HasMember(musicPath.c_str()))
		{
			log->out("[%t] Error: 14. Stopping load.");
			returnValue = 11;
		}
		
		if (packageConfig->get(audio)->HasMember(soundPath.c_str()))
			actualSoundPath = packageConfig->get(audio)->FindMember(soundPath.c_str())->value.GetString();
		if (packageConfig->get(audio)->HasMember(musicPath.c_str()))
			actualMusicPath = packageConfig->get(audio)->FindMember(musicPath.c_str())->value.GetString();
		if (actualSoundPath.back() != '/')
			actualSoundPath += '/';
		if (actualMusicPath.back() != '/')
			actualMusicPath += '/';

		log->out("[%t] Sound path: " + actualSoundPath + '.');
		log->out("[%t] Music path: " + actualMusicPath + '.');

		std::string files(Config_.get("Audio", "Container", "files"));
		if (!packageConfig->get(audio)->HasMember(files.c_str()))
		{
			log->out("[%t] Error: 15. Stopping load.");
			returnValue = 12;
		}
		auto arr = packageConfig->get(audio)->FindMember(files.c_str());

		if (!arr->value.IsArray())
		{
			log->out("[%t] Error: 16. Stopping load.");
			returnValue = 13;
		}

		std::string name(Config_.get("Audio", "FileName", "name"));
		std::string alias(Config_.get("Audio", "FileAlias", "alias"));
		std::string isMusic(Config_.get("Audio", "IsMusic", "isMusic"));

		for (auto it = arr->value.Begin(); it != arr->value.End(); it++)
		{
			if (!it->HasMember(name.c_str()))
				continue;

			log->out("[%t] Preparing to load/open audio: " + std::string(it->FindMember(name.c_str())->value.GetString()));

			if (!it->HasMember(isMusic.c_str()))
			{
				log->out("[%t] Could not find IsMusic property. Skipping.");
				continue;
			}

			if (it->FindMember(isMusic.c_str())->value.GetBool())
			{
				log->out("[%t] Opening music.");
				{
					std::lock_guard<std::mutex> lg(AudioMutex_);
					if (!Audio_.opemMusic(actualMusicPath + +it->FindMember(name.c_str())->value.GetString(),
						(it->HasMember(alias.c_str()) ? it->FindMember(alias.c_str())->value.GetString() : it->FindMember(name.c_str())->value.GetString())))
						log->out("[%t] Error: Could not open music: " + std::string(it->FindMember(name.c_str())->value.GetString()));
					else log->out("[%t] Opening successful.");
				}
			}
			else
			{
				auto entry = archive->GetEntry(actualSoundPath + it->FindMember(name.c_str())->value.GetString());
				if (entry->CanExtract())
				{
					std::istream* rawData = entry->GetDecompressionStream();
					if (!rawData)
					{
						log->out("[%t] Decompression stream is null.");
						continue;
					}

					std::string data;
					while (rawData->good())
						data.push_back(rawData->get());
					data.erase(data.length() - 1);

					log->out("[%t] Loading sound.");

					{
						std::lock_guard<std::mutex> lg(AudioMutex_);
						if (!Audio_.loadBinarySoundBuffer(data,
							(it->HasMember(alias.c_str()) ? it->FindMember(alias.c_str())->value.GetString() : it->FindMember(name.c_str())->value.GetString())))
							log->out("[%t] Error: Could not load sound: " + std::string(it->FindMember(name.c_str())->value.GetString()));
						else log->out("[%t] Loading successful.");
					}
				}
				else
					log->out("[%t] Could not extract sound.");
			}
		}
	}

	log->out("[%t] Stoping internal audio load.");
	returnValue = 0;
}

bool sfe::ResourceManager::loadPackage(const std::string& package)
{
	int results = internalLoad_(package);
	if (results > 0)
		return false;
	return true;
}

std::future<int> sfe::ResourceManager::loadPackageAsync(const std::string& package)
{
	std::future<int> results;
	try
	{
		results = std::async(std::launch::async, &ResourceManager::internalLoad_, this, package);
	}
	catch (const std::system_error&)
	{
		results = std::async(std::launch::any, &ResourceManager::internalLoad_, this, package);
	}
	return results;
}

//bool unloadPackage(const std::string& package);
//std::future<int> unloadPackageAsync(const std::string& package);

//bool unloadAudio(const std::string& name);
//bool unloadTexture(const std::string& name);
//bool unloadConfig(const std::string& name);

sf::Texture* sfe::ResourceManager::getTexture(const std::string& name)
{
	if (name.empty())
		return nullptr;

	std::lock_guard<std::mutex> lg(TexturesMutex_);
	return Textures_.get(name);
}

sf::Font* sfe::ResourceManager::getFont(const std::string& name)
{
	if (name.empty())
		return nullptr;

	std::lock_guard<std::mutex> lg(FontsMutex_);
	return Fonts_.get(name);
}

sf::SoundBuffer* sfe::ResourceManager::getSoundBuffer(const std::string& name)
{
	if (name.empty())
		return nullptr;

	std::lock_guard<std::mutex> lg(AudioMutex_);
	return Audio_.getSoundBuffer(name);
}

sf::Music* sfe::ResourceManager::getMusic(const std::string& name)
{
	if (name.empty())
		return nullptr;

	std::lock_guard<std::mutex> lg(AudioMutex_);
	return Audio_.getMusic(name);
}