#include "AudioMgr.h"

sfe::AudioMgr::AudioMgr()
{
}

sfe::AudioMgr::~AudioMgr()
{
}

bool sfe::AudioMgr::loadSoundBuffer(const std::string& filename, const std::string& name)
{
	if (filename.empty() || name.empty() || SoundBuffers_.find(name) != SoundBuffers_.end())
		return false;

	return SoundBuffers_[name].loadFromFile(filename);
}

bool sfe::AudioMgr::loadBinarySoundBuffer(const std::string& data, const std::string& name)
{
	if (data.empty() || name.empty() || SoundBuffers_.find(name) != SoundBuffers_.end())
		return false;

	return SoundBuffers_[name].loadFromMemory(data.c_str(), data.length());
}

bool sfe::AudioMgr::opemMusic(const std::string& filename, const std::string& name)
{
	if (filename.empty() || name.empty() || Music_.find(name) != Music_.end())
		return false;

	return Music_[name].openFromFile(filename);
}

sf::SoundBuffer* sfe::AudioMgr::getSoundBuffer(const std::string& name)
{
	if (name.empty() || SoundBuffers_.find(name) == SoundBuffers_.end())
		return nullptr;
	return &SoundBuffers_.at(name);
}

sf::Music* sfe::AudioMgr::getMusic(const std::string& name)
{
	if (name.empty() || Music_.find(name) == Music_.end())
		return nullptr;
	return &Music_.at(name);
}