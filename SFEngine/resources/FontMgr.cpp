#include "FontMgr.h"

sfe::FontMgr::FontMgr()
{
}

sfe::FontMgr::~FontMgr()
{
}

bool sfe::FontMgr::load(const std::string& filename, const std::string& name)
{
	if (filename.empty() || name.empty() || Fonts_.find(name) != Fonts_.end())
		return false;

	Fonts_.emplace(name, sf::Font());
	return Fonts_.at(name).loadFromFile(filename);
}


bool sfe::FontMgr::loadBinary(const std::string& data, const std::string& name)
{
	if (data.empty() || name.empty() || Fonts_.find(name) != Fonts_.end())
		return false;

	Fonts_.emplace(name, sf::Font());
	return Fonts_.at(name).loadFromMemory(data.c_str(), data.length());
}

sf::Font* sfe::FontMgr::get(const std::string& name)
{
	if (name.empty() || Fonts_.find(name) == Fonts_.end())
		return nullptr;
	return &Fonts_.at(name);
}