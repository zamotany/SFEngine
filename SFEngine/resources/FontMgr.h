#pragma once

#include "BaseMgr.h"

namespace sfe
{
	class FontMgr;
}

// \brief Unit manager for fonts manipulation.
// \author Pawe� Trys�a aka zamotany.
class sfe::FontMgr : public BaseMgr
{
public:
	// \brief Default constructor.
	FontMgr();

	// \brief Default destructor.
	~FontMgr();

	// \brief Operator =.
	FontMgr& operator=(const FontMgr&) = delete;

	// \breif Copy constructor.
	FontMgr(const FontMgr&) = delete;

	// \brief Load font from file.
	// \param filename Full file name with path.
	// \param name Name of the font for identification.
	// \return False is filename is empty, name is empty, font is already loaded or could not load font, true otherwise.
	bool load(const std::string& filename, const std::string& name);

	// \breif Load font from memory.
	// \param data Binary data representing font.
	// \param name Name of the font for identification.
	// \return False is data is empty, name is empty, font is already loaded or could not load font, true otherwise.
	bool loadBinary(const std::string& data, const std::string& name);

	// \brief Get font specified by name.
	// \param name Name identificator of font.
	// \return Pointer to font.
	sf::Font* get(const std::string& name);
private:
	std::map<sf::String, sf::Font> Fonts_;
};