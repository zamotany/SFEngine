#pragma once

#include <sstream>
#include "../core/JSONParser.h"
#include "../core/INIParser.h"
#include "../core/Logger.h"
#include "../core/FileSystem.h"
#include "ZipArchive.h"
#include "TextureMgr.h"
#include "FontMgr.h"
#include "BitmaskMgr.h"
#include "AudioMgr.h"

namespace sfe
{
	class ResourceManager;
}

// \brief Manager for every resources like textures, audio files, config files
//        and so on.
// \author Pawe� Trys�a aka zamotany.
// \note Statuc codes:
//       1 - Package filename is empty.
//       2 - Could not read package config file.
//       3 - Could not get config's file property.
//       4 - Could not get config's size property.
//       5 - Could not read archive.
//       6 - Archive buffer is empty.
//       7 - Corrupted archive.
//       8 - No textures' path.
//       9 - No textures' files array.
//       10 - Textures' files property is not an array.
//       11 - No fonts' path.
//       12 - No font' files array.
//       13 - Fonts' files property is not an array.
// \note INI configuration file's poroperies:
//       [Debug]
//       ; File to which output will be written. Default: ResourceManager.%t.txt.
//       OutputFile="ResourceManager.%t.txt"
//       [Package]
//       ; Name of property conatining path to package. Default: file.
//       File = "file"
//       ; Name of property conatining package size. Default: size.
//       Size = "size"
//       [Textures]
//       ; Name of property containing all textures' into. Default: textures.
//       Name = "textures"
//       ; Name of property conatining path to textures folder. Default: path.
//       Path = "path"
//       ; Name of property conatining all textures. Default: files.
//       Container = "files"
//       ; Name of property conatining texture's name. Default: name.
//       FileName = "name"
//       ; Name of property conatining texture's alias. Default: alias.
//       FileAlias = "alias"
//       ; Name of property conatining texture's create mask option. Default: createMask.
//       FileCreateMask = "createMask"
//       ; Name of property conatining texture's smooth option. Default: smooth.
//       FileSmooth = "smooth"
//       ; Name of property conatining texture's repeat option. Default: repeat.
//       FileRepeat = "repeat"
//       [Fonts]
//       ; Analogical as above.
//       Name = "fonts"
//       Path = "path"
//       Container = "files"
//       FileName = "name"
//       FileAlias = "alias"
//       [Audio]
//       ; Analogical as above.
//       Name = "fonts"
//       Container = "files"
//       FileName = "name"
//       FileAlias = "alias"
//       ; Name of poprepty containging path to sounds. Default: soundPath.
//       SoundPath = "soundPath"
//       ; Name of poprepty containging path to music. Default: musicPath.
//       ; Since music is not in package this property holds path to folder with music files.
//       MusicPath = "musicPath"
//       ; Name of poprepty containging information if audio is a sound or a music. Default: isMusic.
//       ; If true then audio is a music, if false then it is sound.
//       IsMusic = "isMusic"
class sfe::ResourceManager
{
public:
	// \breif Default constructor.
	// \param config INI configuration file.
	ResourceManager(const std::string& config);

	// \breif Default destructor.
	~ResourceManager();

	// \brief Synchronously read and load package.
	// \param package Full package config file name with path.
	// \return True if load is successful, false otherwise.
	// \note File with log output will be generated for addition information.
	bool loadPackage(const std::string& package);

	// \brief Asynchronously read and load package.
	// \param package Full package config file name with path.
	// \return Future with status code.
	// \note File with log output will be generated for addition information.
	// \note If attemption to read file in new thread fails, file will be read either asynchronously or synchronously
	//       depending on avaible system resource.
	std::future<int> loadPackageAsync(const std::string& package);

	/*bool unloadPackage(const std::string& package);
	std::future<int> unloadPackageAsync(const std::string& package);

	bool unloadAudio(const std::string& name);
	bool unloadTexture(const std::string& name);
	bool unloadConfig(const std::string& name);*/

	// \breif Get texture specified by name.
	// \param name Texture's name.
	// \return Texture's pointer or nullptr if name is empty or could not find.
	sf::Texture* getTexture(const std::string& name);

	// \breif Get font specified by name.
	// \param name Font's name.
	// \return Font's pointer or nullptr if name is empty or could not find.
	sf::Font* getFont(const std::string& name);

	// \breif Get sound buffer specified by name.
	// \param name Sound's name.
	// \return Sound's pointer or nullptr if name is empty or could not find.
	sf::SoundBuffer* getSoundBuffer(const std::string& name);

	// \breif Get music specified by name.
	// \param name Music' name.
	// \return Music' pointer or nullptr if name is empty or could not find.
	sf::Music* getMusic(const std::string& name);

private:
	// \breif Actually load archive content base on package config JSON.
	// \param package Full file name with path to JSON file with package config.
	// \return Status code.
	int internalLoad_(const std::string& package);

	// \breif Load textures from archive.
	// \param packageConfig JSON configuration file.
	// \param archive Archive with files.
	// \param returnValue Variable to which status code of loading will pe stored.
	// \param log Logger instance for debug logging.
	// \note Possible status codes: 8, 9, 10, 0.
	void internalLoadTextures_(JSONParser* packageConfig, ZipArchive* archive, int& returnValue, Logger* log);

	// \breif Load fonts from archive.
	// \param packageConfig JSON configuration file.
	// \param archive Archive with files.
	// \param returnValue Variable to which status code of loading will pe stored.
	// \param log Logger instance for debug logging.
	// \note Possible status codes: 11, 12, 13, 0.
	void internalLoadFonts_(JSONParser* packageConfig, ZipArchive* archive, int& returnValue, Logger* log);

	// \breif Load audio files from archive.
	// \param packageConfig JSON configuration file.
	// \param archive Archive with files.
	// \param returnValue Variable to which status code of loading will pe stored.
	// \param log Logger instance for debug logging.
	// \note Possible status codes: 14, 15, 16, 0.
	void internalLoadAudio_(JSONParser* packageConfig, ZipArchive* archive, int& returnValue, Logger* log);

	TextureMgr Textures_;
	FontMgr Fonts_;
	AudioMgr Audio_;
	INIParser Config_;
	std::mutex TexturesMutex_;
	std::mutex FontsMutex_;
	std::mutex AudioMutex_;
};