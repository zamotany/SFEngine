#pragma once

#include "BaseMgr.h"

namespace sfe
{
	class BitmaskMgr;
}

// \brief Unit manager holding texture's bitmasks.
// \author Pawe� Trys�a aka zamotany.
// \detail This is slightly modified and adjusted version of Bitmasks manager from SFML's wiki.
//         Link here: https://github.com/SFML/SFML/wiki/Source:-Simple-Collision-Detection-for-SFML-2
//         Authors of original version: Nick Koirala (original version), ahnonay (SFML2 compatibility).
class sfe::BitmaskMgr
{
public:
	// \brief Default constructor.
	BitmaskMgr();

	// \breif Default destructor.
	~BitmaskMgr();

	// \breif Operator =.
	BitmaskMgr& operator=(const BitmaskMgr&) = delete;

	// \breif Copy constructor.
	BitmaskMgr(const BitmaskMgr&) = delete;

	// \breif Gets single pixel from texture's mask.
	static sf::Uint8 getPixel(const sf::Uint8* mask, const sf::Texture* tex, unsigned int x, unsigned int y);

	// \breif Creates mask.
	sf::Uint8* createMask(const sf::Texture* tex, const sf::Image& img);

	// \brief Gets mask of specified texture.
	sf::Uint8* getMask(const sf::Texture* tex);
private:
	std::map<const sf::Texture*, sf::Uint8*> Bitmasks_;
};


