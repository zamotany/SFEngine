#pragma once

#include "BaseMgr.h"

namespace sfe
{
	class AudioMgr;
}

// \breif Unit manager for audio files (sound buffers and music files).
// \author Pawe� Trys�a aka zamotany.
class sfe::AudioMgr : public BaseMgr
{
public:
	// \breif Default constructor.
	AudioMgr();

	// \breif Default destructor.
	~AudioMgr();

	// \breif Copy constructor.
	// \note This constructor is deleted.
	AudioMgr(const AudioMgr&) = delete;

	// \breif Operator =.
	AudioMgr& operator=(const AudioMgr&) = delete;

	// \breif Load sound buffer directly from file.
	// \param filename Full file name with path to sound file.
	// \param name Name of sound buffer for identification.
	// \return True if sound buffer was loaded successfully, false otherwise.
	bool loadSoundBuffer(const std::string& filename, const std::string& name);

	// \breif Load sound buffer from array of bytes.
	// \param data Array of bytes.
	// \param name Name of sound buffer for identification.
	// \return True if sound buffer was loaded successfully, false otherwise.
	bool loadBinarySoundBuffer(const std::string& data, const std::string& name);

	// \breif Open music from file.
	// \param filename Full file name with path to music file.
	// \param name Name of music for identification.
	// \return True if music was opened successfully, false otherwise.
	// \note Music is streamed from file, not entirely loaded into memory.
	bool opemMusic(const std::string& filename, const std::string& name);

	// \breif Get sound buffer with given name.
	// \param name Name of sound buffer.
	// \return Pointer to sound buffer.
	sf::SoundBuffer* getSoundBuffer(const std::string& name);

	// \breif Get music with given name.
	// \param name Name of music
	// \return Pointer to music.
	sf::Music* getMusic(const std::string& name);
private:
	std::map<std::string, sf::SoundBuffer> SoundBuffers_;
	std::map<std::string, sf::Music> Music_;
};