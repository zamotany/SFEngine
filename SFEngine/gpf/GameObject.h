#pragma once

#include <memory>
#include <string>
#include <map>
#include "Sprite.h"
#include "Renderer.h"
#include "Animator.h"
#include "BoxCollider.h"
#include "CircleCollider.h"
#include "PixelCollider.h"
#include "Script.h"

namespace sfe
{
	class GameObject;
}

// \breif Game object.
// \author Pawe� Trys�a
class sfe::GameObject
{
public:
	// \breif Default constructor.
	GameObject();

	// \breif Default destructor.
	~GameObject();

	// \breif Check if game object has Sprite component.
	// \return True if game object has component, false otherwise.
	bool hasSprite() const;

	// \breif Check if game object has Renderer component.
	// \return True if game object has component, false otherwise.
	bool hasRenderer() const;

	// \breif Check if game object has Animator component.
	// \return True if game object has component, false otherwise.
	bool hasAnimator() const;

	// \breif Check if game object has Collider component.
	// \return True if game object has component, false otherwise.
	bool hasCollider() const;

	// \breif Check if game object has any Script component.
	// \return True if game object has any component, false otherwise.
	bool hasScript() const;

	// \breif Get flags with component.
	// \return Flags with components.
	// \note Use bitwise AND and Component::Type enum class casted as integer.
	unsigned short getComponents() const;

	// \brief Get scripts quanity.
	// \return Scripts quanity.
	size_t scriptQuanity() const;

	// \breif Add Sprite component.
	void addSprite();

	// \breif Add Renderer component.
	void addRenderer();

	// \breif Add Animator component.
	void addAnimator();

	// \breif Add Box Collider component.
	void addBoxCollider();

	// \breif Add Circle Collider component.
	void addCircleCollider();

	// \breif Add Pixel Collider component.
	void addPixelCollider();

	// \breif Add Script component.
	// \param name Name of the script.
	// \param script Unique pointer with script instance.
	// \note Use std::make_unique to create an instane of script.
	void addScript(const std::string& name, std::unique_ptr<Script> script);

	// \breif Get pointer to Sprite component.
	// \return Pointer to component if exists, nullptr otherwise.
	Sprite* getSprite();

	// \breif Get pointer to Renderer component.
	// \return Pointer to component if exists, nullptr otherwise.
	Renderer* getRenderer();

	// \breif Get pointer to Animator component.
	// \return Pointer to component if exists, nullptr otherwise.
	Animator* getAnimator();

	// \breif Get pointer to Collider component.
	// \return Pointer to component if exists, nullptr otherwise.
	Collider* getCollider();

	// \breif Get pointer to Script component.
	// \return Pointer to component if exists, nullptr otherwise.
	Script* getScript(const std::string& name);
private:
	unsigned short Components_;
	std::unique_ptr<Sprite> Sprite_;
	std::unique_ptr<Renderer> Renderer_;
	std::unique_ptr<Animator> Animator_;
	std::unique_ptr<Collider> Collider_;
	std::map<std::string, std::unique_ptr<Script>> Scripts_;
};