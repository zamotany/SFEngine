#include "Component.h"

sfe::Component::Component(Type type) : Type_(type), Enabled_(true)
{
}

sfe::Component::~Component()
{
}

void sfe::Component::disable()
{
	Enabled_ = false;
}

void sfe::Component::enable()
{
	Enabled_ = true;
}