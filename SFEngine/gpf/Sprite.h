#pragma once

#include "Component.h"

namespace sfe
{
	class Sprite;
}

// \brief Sprite component holding visual representation of object.
// \author Pawe� Trys�a aka zamotany
// \note Since sprite component actually does nothing, it cannot be disabled.
class sfe::Sprite : public sfe::Component
{
public:
	// \brief SFML's sprite object.
	sf::Sprite SpriteObject;

	// \brief Default constructor.
	Sprite();

	// \brief Default destructor.
	~Sprite();

	// \brief Initialise component.
	// \note This method does nothing.
	void init();

	// \brief Set up component for looping.
	// \note This method does nothing.
	void begin();

	// \brief Update component.
	// \param delta Delta time.
	// \note This method does nothing.
	void update(sf::Time delta);
};