#include "Renderer.h"

sfe::Renderer::Renderer(Sprite* sprite) :
	Component(Component::Type::RENDER), SpriteComponent_(sprite)
{
	if (SpriteComponent_)
	{
		RT_.create(
			SpriteComponent_->SpriteObject.getGlobalBounds().width,
			SpriteComponent_->SpriteObject.getGlobalBounds().height
			);
	}
}

sfe::Renderer::~Renderer()
{
}

void sfe::Renderer::reset()
{
	if (SpriteComponent_)
	{
		RT_.create(
			SpriteComponent_->SpriteObject.getGlobalBounds().width,
			SpriteComponent_->SpriteObject.getGlobalBounds().height
			);
	}
}

void sfe::Renderer::draw(sf::RenderTarget& target, sf::RenderStates states)
{
	if (!Enabled_)
		return;

	if (Shader && SpriteComponent_)
	{
		RT_.clear();
		RT_.draw(SpriteComponent_->SpriteObject, Shader.get());
		RT_.display();
		RTSprite_.setTexture(RT_.getTexture());
		RTSprite_.setPosition(SpriteComponent_->SpriteObject.getPosition());
		target.draw(RTSprite_, states);
	}
	else if (SpriteComponent_)
	{
		target.draw(SpriteComponent_->SpriteObject, states);
	}

}

void sfe::Renderer::init()
{
}

void sfe::Renderer::begin()
{
}

void sfe::Renderer::update(sf::Time delta)
{
}