#pragma once

#include <SFML\Graphics.hpp>

namespace sfe
{
	class Component;
}

// \breif Base abstract class for every Component.
// \author Pawe� Trys�a aka zamotany
class sfe::Component
{
public:
	// \breif Component type.
	enum class Type : int
	{
		UNDEFINED = 0,
		SPRITE = 1,
		RENDER = 1 << 1,
		ANIMATOR = 1 << 2,
		COLLIDER = 1 << 3,
		SCRIPT = 1 << 4
	};

	// \breif Default constructor.
	// \param type Component type.
	Component(Type type);

	// \breif Default destructor.
	virtual ~Component();

	// \breif Initialise component.
	virtual void init() = 0;

	// \breif Prepare component for core game loop.
	virtual void begin() = 0;

	// \breif Update component.
	// \param delta Delta time.
	virtual void update(sf::Time delta) = 0;

	// \breif Enable component.
	void enable();

	// \breif Disable component.
	void disable();
protected:
	Type Type_;
	bool Enabled_;
};