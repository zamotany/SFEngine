#include "Animator.h"

sfe::Animator::Animator(Sprite* sprite) :
Component(Type::ANIMATOR), SpriteComponent_(sprite), CurrentFrame_(-1), FrameSize_(0, 0), Accumulator_(sf::Time::Zero)
{
}

sfe::Animator::~Animator()
{
}

void sfe::Animator::setFrameSize(unsigned int width, unsigned int height)
{
	FrameSize_.x = width;
	FrameSize_.y = height;
}

void sfe::Animator::addAnimation(const std::string& name, size_t startFrame, size_t endFrame, size_t framesPerSecond, bool loop)
{
	if (name.empty() || startFrame == -1 || endFrame == -1 || framesPerSecond < 1)
		return;

	Animations_[name].StartFrame = startFrame;
	Animations_[name].EndFrame = endFrame;
	Animations_[name].FramesPerSecond = framesPerSecond;
	Animations_[name].Loop = loop;
}

void sfe::Animator::setFrame(size_t frame)
{
	CurrentFrame_ = frame;
}

size_t sfe::Animator::getFrame() const
{
	return CurrentFrame_;
}

void sfe::Animator::play(const std::string& name)
{
	auto pos = Animations_.find(name);
	if (pos != Animations_.end())
	{
		CurrentAnim_ = pos->second;
		CurrentFrame_ = CurrentAnim_.StartFrame;
		setTexture();
	}
}

void sfe::Animator::stop()
{
	CurrentFrame_ = -1;
	CurrentAnim_ = { -1, -1, -1, false };
}

void sfe::Animator::init()
{
}

void sfe::Animator::begin()
{
	setTexture();
}

void sfe::Animator::setTexture()
{
	if (FrameSize_.x == 0 || FrameSize_.y == 0 || !SpriteComponent_ || CurrentFrame_ == -1)
	{
		SpriteComponent_->SpriteObject.setTextureRect(sf::IntRect(0, 0, 0, 0));
		return;
	}

	auto tex = SpriteComponent_->SpriteObject.getTexture();
	if (tex)
	{
		size_t row = CurrentFrame_ / (tex->getSize().x / FrameSize_.x);
		size_t col = CurrentFrame_ % (tex->getSize().x / FrameSize_.x);
		if (row > tex->getSize().y / FrameSize_.y)
		{
			SpriteComponent_->SpriteObject.setTextureRect(sf::IntRect(0, 0, 0, 0));
			return;
		}

		SpriteComponent_->SpriteObject.setTextureRect(
			sf::IntRect(col * FrameSize_.x, row * FrameSize_.y, FrameSize_.x, FrameSize_.y)
			);
	}
	else
		SpriteComponent_->SpriteObject.setTextureRect(sf::IntRect(0, 0, 0, 0));
}

void sfe::Animator::update(sf::Time delta)
{
	if (FrameSize_.x == 0 || FrameSize_.y == 0 || !SpriteComponent_ || CurrentFrame_ == -1)
		return;

	while (Accumulator_ >= sf::seconds(1.f / (float)CurrentAnim_.FramesPerSecond))
	{
		Accumulator_ -= sf::seconds(1.f / (float)CurrentAnim_.FramesPerSecond);

		if (CurrentFrame_ == CurrentAnim_.EndFrame && CurrentAnim_.Loop)
			CurrentFrame_ = CurrentAnim_.StartFrame;
		else if (CurrentFrame_ < CurrentAnim_.EndFrame)
			CurrentFrame_++;

		setTexture();

	}

	Accumulator_ += delta;
}