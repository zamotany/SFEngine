#pragma once

#include "Component.h"
#include "Animator.h"
#include "Renderer.h"
#include "Collider.h"

namespace sfe
{
	class Script;
}

class sfe::Script : public sfe::Component
{
public:
	Script();
	~Script();
	void onMoveInit(Sprite* sprite, Renderer* renderer, Animator* animator, Collider* collider);
};