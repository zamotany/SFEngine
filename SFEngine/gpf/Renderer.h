#pragma once

#include "Component.h"
#include "Sprite.h"
#include <memory>

namespace sfe
{
	class Renderer;
}

// \brief Renderer component which allows engine to render
//        game object's sprite.
// \author Pawe� Trys�a aka zamotany.
class sfe::Renderer : public sfe::Component, public sf::Drawable
{
public:
	// \brief Shader applied to object on rendering.
	// \note Sprite is given it's own Render Texture, so shader is
	//       applied only to this object.
	std::unique_ptr<sf::Shader> Shader;

	// \brief Default constructor.
	// \param sprite Sprite component of game object.
	Renderer(Sprite* sprite);

	// \brief Default destructor.
	~Renderer();

	// \brief Reset render texture.
	// \note It basically recreates render texture.
	void reset();

	// \brief Implementation of abstract methods from Component class.
	// \note Does nothing.
	void init();
	void begin();
	void update(sf::Time delta);
private:
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states);

	sf::RenderTexture RT_;
	sf::Sprite RTSprite_;
	Sprite* SpriteComponent_;
};