#pragma once

#include "Collider.h"

namespace sfe
{
	class PixelCollider;
}

class sfe::PixelCollider : public sfe::Collider
{
public:
	PixelCollider(Sprite* sprite);
	~PixelCollider();
};