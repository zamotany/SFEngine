#pragma once

#include "Collider.h"

namespace sfe
{
	class BoxCollider;
}

class sfe::BoxCollider : public sfe::Collider
{
public:
	BoxCollider(Sprite* sprite);
	~BoxCollider();
};