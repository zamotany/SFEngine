#pragma once

#include "Component.h"
#include "Sprite.h"
#include <string>
#include <map>

namespace sfe
{
	class Animator;
}

// \brief Sprite animator component.
// \author Pawe� Trys�a aka zamotany.
class sfe::Animator : public sfe::Component
{
public:
	// \brief Default constructor.
	// \param sprite Sprite component.
	Animator(Sprite* sprite);

	// \brief Default destructor.
	~Animator();

	// \brief Set frame size.
	// \note Need to be set before attemptiong to play an animation.
	// \param width Width of frame.
	// \param height Height of frame.
	void setFrameSize(unsigned int width, unsigned int height);

	// \brief Add animation.
	// \param name Name of the animation.
	// \param startFrame Starting frame of animation.
	// \param endFrame Ending frame of animation.
	// \param framesPerSecond Frequency of displaying animation.
	// \param loop Loop the animation. Default is false.
	// \note If animation with given name already exists, it is overwritten.
	// \note Addition fails if either of the following occurs:
	//       name is empty,
	//       startFrame is -1,
	//       endFrame is -1,
	//       framesPerSecond is less than 1.
	void addAnimation(const std::string& name, size_t startFrame, size_t endFrame, size_t framesPerSecond, bool loop = false);

	// \brief Set frame of active animation.
	// \param frame Index of frame.
	void setFrame(size_t frame);

	// \brief Get current frame from active animation.
	// \return Current frame if there is active animation, -1 otherwise.
	size_t getFrame() const;

	// \brief Play an animation.
	// \note Method does nothing if animation was not found.
	// \param name Name of the animation.
	void play(const std::string& name);

	// \brief Stop current animation.
	void stop();

	// \brief Update component.
	// \param delta Delta time.
	void update(sf::Time delta);

	// \brief Initialise component.
	void init();

	// \brief Prepare component for looping.
	// \note First texture's frame are applied.
	void begin();
private:
	void setTexture();

	struct Animation
	{
		size_t StartFrame;
		size_t EndFrame;
		size_t FramesPerSecond;
		bool Loop;
	};
	Sprite* SpriteComponent_;
	sf::Time Accumulator_;
	Animation CurrentAnim_;
	size_t CurrentFrame_;
	sf::Vector2u FrameSize_;
	std::map<std::string, Animation> Animations_;
};