#include "GameObject.h"

sfe::GameObject::GameObject() : Components_(0)
{
}

sfe::GameObject::~GameObject()
{
}

bool sfe::GameObject::hasSprite() const
{
	return static_cast<bool>(Components_ & static_cast<int>(Component::Type::SPRITE));
}

bool sfe::GameObject::hasRenderer() const
{
	return static_cast<bool>(Components_ & static_cast<int>(Component::Type::RENDER));
}

bool sfe::GameObject::hasAnimator() const
{
	return static_cast<bool>(Components_ & static_cast<int>(Component::Type::ANIMATOR));
}

bool sfe::GameObject::hasCollider() const
{
	return static_cast<bool>(Components_ & static_cast<int>(Component::Type::COLLIDER));
}

bool sfe::GameObject::hasScript() const
{
	return static_cast<bool>(Components_ & static_cast<int>(Component::Type::SCRIPT));
}

unsigned short sfe::GameObject::getComponents() const
{
	return Components_;
}

size_t sfe::GameObject::scriptQuanity() const
{
	return Scripts_.size();
}

void sfe::GameObject::addSprite()
{
	if (!hasSprite())
		Components_ |= static_cast<int>(Component::Type::SPRITE);
	Sprite_ = std::make_unique<Sprite>();
}

void sfe::GameObject::addRenderer()
{
	if (!hasRenderer())
		Components_ |= static_cast<int>(Component::Type::RENDER);
	Renderer_ = std::make_unique<Renderer>(Sprite_.get());
}

void sfe::GameObject::addAnimator()
{
	if (!hasAnimator())
		Components_ |= static_cast<int>(Component::Type::ANIMATOR);
	Animator_ = std::make_unique<Animator>(Sprite_.get());
}

void sfe::GameObject::addBoxCollider()
{
	if (!hasCollider())
		Components_ |= static_cast<int>(Component::Type::COLLIDER);
	Collider_ = std::make_unique<BoxCollider>(Sprite_.get());
}

void sfe::GameObject::addCircleCollider()
{
	if (!hasCollider())
		Components_ |= static_cast<int>(Component::Type::COLLIDER);
	Collider_ = std::make_unique<CircleCollider>(Sprite_.get());
}

void sfe::GameObject::addPixelCollider()
{
	if (!hasCollider())
		Components_ |= static_cast<int>(Component::Type::COLLIDER);
	Collider_ = std::make_unique<PixelCollider>(Sprite_.get());
}

void sfe::GameObject::addScript(const std::string& name, std::unique_ptr<Script> script)
{
	if (!hasScript())
		Components_ |= static_cast<int>(Component::Type::SCRIPT);
	Scripts_[name] = std::move(script);
	Scripts_[name]->onMoveInit(Sprite_.get(), Renderer_.get(), Animator_.get(), Collider_.get());
}

sfe::Sprite* sfe::GameObject::getSprite()
{
	return Sprite_.get();
}

sfe::Renderer* sfe::GameObject::getRenderer()
{
	return Renderer_.get();
}

sfe::Animator* sfe::GameObject::getAnimator()
{
	return Animator_.get();
}

sfe::Collider* sfe::GameObject::getCollider()
{
	return Collider_.get();
}

sfe::Script* sfe::GameObject::getScript(const std::string& name)
{
	if (Scripts_.find(name) == Scripts_.end())
		return nullptr;
	return Scripts_.at(name).get();
}