#pragma once

#include "Collider.h"

namespace sfe
{
	class CircleCollider;
}

class sfe::CircleCollider : public sfe::Collider
{
public:
	CircleCollider(Sprite* sprite);
	~CircleCollider();
};